import pandas as pd
import os.path

INNER_BARCODES = config["inner_barcodes"]
BARCODE_PAIRS = config["barcodes_pairs"]
REF = config["ref"]
INDEX = os.path.basename(REF)+".mmi"
ERROR_RATE = config["error_cutadap"]
MINLEN = config["min_length_read"]
MAXLEN = config["max_length_read"]
OUTDIR = config["outdir"]
RAW_READS = pd.read_csv(config["raw_reads"],sep="\t",dtype = str)
NATIVE_BARCODES = RAW_READS['barcode'].tolist()

rule all:
  input:
    [OUTDIR+"/consensus_"+NATIVE_BARCODE+".fasta" for NATIVE_BARCODE in NATIVE_BARCODES]+[OUTDIR+"/consensus_"+NATIVE_BARCODE+"_hp.fasta" for NATIVE_BARCODE in NATIVE_BARCODES]
    
rule generate_barcode_files:
  input: 
    inner = INNER_BARCODES, 
    pairs = BARCODE_PAIRS
  output: [OUTDIR+"/"+str(NATIVE_BARCODE)+"_link.fasta" for NATIVE_BARCODE in NATIVE_BARCODES]
  script:
    "scripts/generate_barcode_files.R" 
    
def get_fq(wildcards):
    idx = RAW_READS.index[(RAW_READS['barcode'] == wildcards.NATIVE_BARCODE)].tolist()
    return(RAW_READS['path'].loc[idx].tolist())

checkpoint demultiplex:
  input:
    reads = get_fq,
    bc_file = OUTDIR+"/{NATIVE_BARCODE}_link.fasta"
  params:
    e = ERROR_RATE,
    m = MINLEN,
    M = MAXLEN
  output:
    directory(OUTDIR+"/demultiplex/{NATIVE_BARCODE}")
  shell:
    """
    scripts/demultiplex.sh {params.e} {params.m} {params.M} {input.bc_file} {output} {input.reads} 
    """

rule consensus_medaka:
  input:
    reads = OUTDIR+"/demultiplex/{NATIVE_BARCODE}/trimmed-{SAMPLE}.fastq.gz",
    ref = REF
  output:
    bam = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/calls_to_draft.bam",
    bai = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/calls_to_draft.bam.bai",
    fasta = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/consensus.fasta"
  shell:
    """
    cp {input.ref} {OUTDIR}/{wildcards.NATIVE_BARCODE}_{wildcards.SAMPLE}_ref.fasta
    medaka_consensus -i {input.reads} -d {OUTDIR}/{wildcards.NATIVE_BARCODE}_{wildcards.SAMPLE}_ref.fasta -o {OUTDIR}/medaka/consensus_{wildcards.NATIVE_BARCODE}_{wildcards.SAMPLE} -t $(nproc)
    rm {OUTDIR}/{wildcards.NATIVE_BARCODE}_{wildcards.SAMPLE}_ref.fasta*
    """
    
def aggregate_input_medaka(wildcards):
    checkpoint_output = checkpoints.demultiplex.get(**wildcards).output[0]
    return expand(OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/consensus.fasta",
           NATIVE_BARCODE=wildcards.NATIVE_BARCODE,
           SAMPLE=glob_wildcards(os.path.join(checkpoint_output, "trimmed-{SAMPLE}.fastq.gz")).SAMPLE)
           
rule aggregate_medaka:
    input:
        aggregate_input_medaka
    output:
        OUTDIR+"/consensus_{NATIVE_BARCODE}.fasta"
    script:
        "scripts/aggregate_consensus.R"

rule medaka_variant:
  input:
    bam = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/calls_to_draft.bam",
    bai = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}/calls_to_draft.bam.bai",
    ref = REF
  output:
    bam = OUTDIR+"/medaka/variant_{NATIVE_BARCODE}_{SAMPLE}/round_0_hap_mixed_phased.bam",
    vcf1 = OUTDIR+"/medaka/variant_{NATIVE_BARCODE}_{SAMPLE}/round_1_hap_1.vcf",
    vcf2 = OUTDIR+"/medaka/variant_{NATIVE_BARCODE}_{SAMPLE}/round_1_hap_2.vcf"
  shell:
    """
    medaka_variant -f {input.ref} -i {input.bam} -o {OUTDIR}/medaka/variant_{wildcards.NATIVE_BARCODE}_{wildcards.SAMPLE}
    """
    
rule consensus_medaka_phasing:
  input:
    bam = OUTDIR+"/medaka/variant_{NATIVE_BARCODE}_{SAMPLE}/round_0_hap_mixed_phased.bam",
    ref = REF
  output:
    cons1 = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}_HP1/consensus.fasta",
    cons2 = OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}_HP2/consensus.fasta"
  shell:
    """
    scripts/phasing.sh {input.bam} {input.ref} {OUTDIR} {wildcards.NATIVE_BARCODE} {wildcards.SAMPLE}
    """

def aggregate_input_medaka_hp(wildcards):
    checkpoint_output = checkpoints.demultiplex.get(**wildcards).output[0]
    return expand([OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}_HP1/consensus.fasta",OUTDIR+"/medaka/consensus_{NATIVE_BARCODE}_{SAMPLE}_HP2/consensus.fasta"],
           NATIVE_BARCODE=wildcards.NATIVE_BARCODE,
           SAMPLE=glob_wildcards(os.path.join(checkpoint_output, "trimmed-{SAMPLE}.fastq.gz")).SAMPLE)

rule aggregate_medaka_hp:
    input:
        aggregate_input_medaka_hp
    output:
        OUTDIR+"/consensus_{NATIVE_BARCODE}_hp.fasta"
    script:
        "scripts/aggregate_consensus.R"
    
