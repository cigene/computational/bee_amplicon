#!/bin/bash
#SBATCH --ntasks=4             
#SBATCH --nodes=1   
#SBATCH --partition=verysmallmem

module load anaconda3 #will call R with my pre-installed pacakges

source activate ~/miniconda3/envs/myConda #activate myConda environment to use snakemake, cutadapt, medaka

snakemake --configfile config.yaml --cores 4 -s Snakefile.py 

  
