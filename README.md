## Softwares requirements
The pipeline needs the following softwares:

- snakemake version >= 5.6.0

- cutadapt version >= 2.10

- medaka version 1.0.3

- R with the following installed packages: Biostrings, readr, dplyr

## Input files requirements

- A file contains inner barcode sequences (see `96_barcodes.tsv`)

- A file that specifies the 2 forward/reverse inner barcodes and native barcode for each sample (see `pairs.tsv`)

- A file contains the path of raw reads where each file contains the reads of samples having the same native barcode (see `reads.tsv`).

- A reference sequence (ex: `Vg_sequence.fasta`)

- A config file that defines options, parameters (see `config.yaml`)

## What the pipeline does and the output results

- Demultiplexing: Demultiplex each native barcode file into separate samples using [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html#linked-adapters). Users can specify the error rate, the minimum and maximum lengths of reads after trimming inner barcodes. The demultiplexed reads will be written to the sub-folder **demultiplex** of the output folder.

- Generate consensus sequence for each demultiplexed sample: Use [medaka_consensus](https://github.com/nanoporetech/medaka) to generate consensus sequence for each sample based on the given reference sequencee. The results of each sample are written into the sub-folder **medaka/consensus_SAMPLE**. Then the consensus sequences of all samples having the same native barcode are put together into one file **consensus_NATIVEBARCODE.fasta**

- Call snp and indel for each haplotype of each sample: Use the variant calling pipeline of [medaka](https://nanoporetech.github.io/medaka/snp.html) to call snp and indel for each haplotype of each demultiplexed sample regarding the reference sequence. The results of each sample are written into the sub-folder **medaka/variant_SAMPLE**.

- Generate haplotype consensus sequence for each sample: Use the phased alignments of *medaka_variant* pipeline above to separate reads coming for different haplotypes of each sample. Then use *medaka_consensus* again to generate consensus sequence for each haplotype from those separated reads. The results of each sample are written into the sub-folder **medaka/consensus_SAMPLE_HP1** and **medaka/consensus_SAMPLE_HP2**. The haplotype consensus sequences of all samples having the same native barcode are put together into one file **consensus_NATIVEBARCODE_hp.fasta**

<img src="bee_amplicon_pipeline.jpg" alt="The workflow" width="800"/>

## Running the pipepline

- Clone the pipeline: `git clone https://gitlab.com/cigene/computational/bee_amplicon.git`

- Install required softwares if necessary. The easiest way is to use [miniconda](https://docs.conda.io/en/latest/miniconda.html):

```
conda create -n myConda -c conda-forge -c bioconda medaka
conda activate myConda
conda install cutadapt
conda install -c conda-forge mamba
mamba install -c bioconda snakemake
```

- Install the necessary R packages if you haven't got them.

- Add/edit the input files to adapt to your input data. Otherwise, the pipeline will run on the test data.

- Edit the file `run.sh` for loading softwares and the number of cores (*#SBATCH --ntasks* and *--cores* in the snakemake command). Ideally the number of cores can be set to be equal to the number of samples so that each job (calling variant, generating consensus sequence) of each sample can be run in one core. But less cores is fine.

- Submit the job (`sbatch run.sh`)


