library(readr)
library(dplyr)
library(Biostrings)
barcodes <- read_tsv(snakemake@input[["inner"]])
barcodes_comb <- read_tsv(snakemake@input[["pairs"]])
allNativeBarCodes <- unique(barcodes_comb$NativeBarcode)
for (bc in allNativeBarCodes){
  bc_file <- paste0(dirname(snakemake@output[[1]]),"/",bc,"_link.fasta")
  file.create(bc_file)
  barcodes_bc <- filter(barcodes_comb, NativeBarcode == bc)
  for (i in 1:nrow(barcodes_bc)){
    cat(">",barcodes_bc$Sample[i],"\n", file = bc_file, append = T,sep = "")
    id <- which(barcodes$Barcode == barcodes_bc$ForwardBC[i])
    fw <- barcodes$Sequence[id]
    id <- which(barcodes$Barcode == barcodes_bc$ReverseBC[i])
    rv <- as.character(reverseComplement(DNAString(barcodes$Sequence[id])))
    cat(fw,"AGCCGAAT...TCTTTCGT",rv,"\n",file = bc_file, append = T, sep = "")
  }
}
