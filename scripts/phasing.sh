input_bam=$1
input_ref=$2
OUTDIR=$3
NATIVE_BARCODE=$4
SAMPLE=$5
samtools view -H ${input_bam} > ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/header.sam
samtools view ${input_bam} | grep 'HP:i:1' >  ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.body.sam
samtools view ${input_bam} | grep 'HP:i:2'  >  ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.body.sam
cat ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/header.sam ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.body.sam | samtools view -bS - > ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.bam
cat ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/header.sam ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.body.sam | samtools view -bS - > ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.bam
rm ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/*sam
samtools bam2fq ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.bam > ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.fastq.gz
samtools bam2fq ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.bam > ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.fastq.gz
nbL=$(wc -l ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.fastq.gz | cut -f 1 -d " ")
if [ $nbL == 0 ]; then 
  cp ${OUTDIR}/demultiplex/${NATIVE_BARCODE}/trimmed-${SAMPLE}.fastq.gz ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.fastq.gz
  cp ${OUTDIR}/demultiplex/${NATIVE_BARCODE}/trimmed-${SAMPLE}.fastq.gz ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.fastq.gz
fi
cp ${input_ref} ${OUTDIR}/${NATIVE_BARCODE}_${SAMPLE}_ref_phasing.fasta
medaka_consensus -i ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP1.fastq.gz -d ${OUTDIR}/${NATIVE_BARCODE}_${SAMPLE}_ref_phasing.fasta -o ${OUTDIR}/medaka/consensus_${NATIVE_BARCODE}_${SAMPLE}_HP1 -t $(nproc)
medaka_consensus -i ${OUTDIR}/medaka/variant_${NATIVE_BARCODE}_${SAMPLE}/HP2.fastq.gz -d ${OUTDIR}/${NATIVE_BARCODE}_${SAMPLE}_ref_phasing.fasta -o ${OUTDIR}/medaka/consensus_${NATIVE_BARCODE}_${SAMPLE}_HP2 -t $(nproc)
rm ${OUTDIR}/${NATIVE_BARCODE}_${SAMPLE}_ref_phasing.fasta*