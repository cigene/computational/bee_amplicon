OUTDIR=$5
mkdir -p $OUTDIR
cutadapt --revcomp -e $1 -m $2 -M $3 -g file:$4 -o ${OUTDIR}/trimmed-{name}.fastq.gz $6 
mv ${OUTDIR}/trimmed-unknown.fastq.gz ${OUTDIR}/$(basename $OUTDIR)-unknown.fastq.gz
